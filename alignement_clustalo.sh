#!/bin/bash

#Création du dossier qui va accueillir les fichiers de sortie
mkdir clustal_100groupes

#boucle qui parcours un dossier (mis en paramètre) contenant les différentes jeu de donnée.
for f in $( ls ${1} )
do 
	#Alignement multiple avec muscle pour chaque fichier
	clustalo -i ${1}/$f -o clustal_100groupes/$f -v
done


