#!/in/bash

#reation d'un dossier qui va accueillir les fichiers de sortie
mkdir muscle_75groupes



#boucle qui parcours un dossier (mis en paramètre) contenant les différentes jeu de donnée.
for f in $( ls ${1} )
do 
	#Alignement multiple avec muscle pour chaque fichier
	muscle -in ${1}/$f -out muscle_75groupes/$f
done

